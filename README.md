# Notes Website

Website for serving markdown notes. May be used for mobile viewing and sharing purposes. [Obsidian](https://obsidian.md/) is good for managing notes on desktop.

**Features:**
- Automatic markdown website generation.
- Authorization: only authorized user can see all notes.
- Note sharing: some notes can be viewed by anyone.

## Usage

1. Create Dockerfile:

```dockerfile
FROM registry.gitlab.com/atnartur/notes-website:latest

COPY . /app/notes
```

2. Create docker-compose.yml

```yaml
version: '2'

services:
  notes:
    build:
      context: .
      dockerfile: Dockerfile
    environment:
      AUTH_USER: USER # enter your username
      AUTH_PASSWORD: PASS # enter your password
    ports:
      - 80:80
```

3. Run `docker-compose up`
4. Open http://localhost

You can deploy this container on your server.

## Note sharing

You can create a list of shared notes that can be viewed without authorization.

1. Create `shared.md`
2. Add names of shared notes (one name on one line)
3. Then go to public link in following format: `http://hostname/shared/NOTE_NAME`. 

Example: note "How start notes website" will display on  `http://example.com/shared/How start notes website`.

## Technical details

Project based on
- [Eleventy](https://www.11ty.dev/) for build html from markdown files.
- [awsm.css](https://igoradamenko.github.io/awsm.css/).
- [highlight.js](https://highlightjs.org/) for code highlighting.
- [Docker](https://docker.com) & [Gitlab CI](https://gitlab.com/ci) for build and deploy.
- [nginx](https://nginx.org) for hosting and authorization.

