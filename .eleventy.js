const fs = require('fs');
const path = require('path');
const markdownIt = require("markdown-it");
const markdownItReplaceLink = require('markdown-it-replace-link');
const pluginTOC = require('eleventy-plugin-nesting-toc');
const markdownItAnchor = require('markdown-it-anchor');

const INPUT_DIR = 'notes/';
const OUTPUT_DIR = 'dist/';

/**
 * Copy index.md for main page with list of notes
 */
function copyIndexFile() {
	const indexDest = path.join(__dirname, INPUT_DIR, 'index.md');
	if (!fs.existsSync(indexDest)) {
		fs.copyFileSync(path.join(__dirname, 'index.md'), indexDest);
	}	
}

/**
 * Create symlinks for shared notes
 * @return {[type]} [description]
 */
function makeSharedNotesSymlinks() {
	const sharedPath = path.join(__dirname, INPUT_DIR, 'shared.md');
	// if (!fs.existsSync(sharedPath)) {
	// 	return;
	// }

	const sharedFile = fs.readFileSync(sharedPath).toString();
	const notes = sharedFile.trim().split('\n').map(x => x.replace('[[', '').replace(']]', ''));
	
	const sharedOutputDirPath = path.join(__dirname, OUTPUT_DIR, 'shared');
	fs.rmdirSync(sharedOutputDirPath, {recursive: true});
	fs.mkdirSync(sharedOutputDirPath);

	notes.forEach(note => {
		const src = path.join('..', note);	
		const dest = path.join(sharedOutputDirPath, note);
		if (!fs.existsSync(dest)) {
			fs.symlinkSync(src, dest);
		}
	});
}

module.exports = function(eleventyConfig) {
	copyIndexFile();

	eleventyConfig.addPlugin(pluginTOC, {
		wrapper: 'div',
		tags: ['h1', 'h2', 'h3', 'h4', 'h5', 'h6']
	});

    eleventyConfig.addPassthroughCopy("robots.txt");
    eleventyConfig.addPassthroughCopy({
    	"node_modules/awsm.css/dist/awsm_theme_big-stone.min.css": "css/awsm_theme_big-stone.min.css",
    	"node_modules/@highlightjs/cdn-assets/styles/monokai-sublime.min.css": "css/highlight-monokai-sublime.min.css",
    	"node_modules/@highlightjs/cdn-assets/highlight.min.js": "js/highlight.min.js"
    });
    eleventyConfig.setUseGitIgnore(false);	

    const markdownItOptions = {
    	breaks: true,
    	linkify: true,
    	replaceLink: function (link, env) {
    		if (link.startsWith('images')) {
    			// image inlining
    			const img = fs.readFileSync(path.join(__dirname, INPUT_DIR, link));
    			const buff = new Buffer(img);
    			const linkArr = link.split('.');
    			const format = linkArr[linkArr.length - 1];
    			const base64 = buff.toString('base64');
    			return `data:image/${format};base64,${base64}`;
    		}
	        return link;
	    }
    };
    const markdownItInstance = markdownIt(markdownItOptions).use(markdownItReplaceLink).use(markdownItAnchor, {});
    eleventyConfig.setLibrary("md", markdownItInstance);

    eleventyConfig.on('afterBuild', () => makeSharedNotesSymlinks());

    eleventyConfig.addCollection("notes", function(collectionApi) {
    	return collectionApi.getAll().reverse().filter(function(item) {
      		return !item.data.page.fileSlug.includes('shared');
    	});
  	});

	return {
		jsDataFileSuffix: "../_data",
		templateFormats: ['md', 'png', 'jpg', 'gif'],
		dir: {
			input: INPUT_DIR,
			output: OUTPUT_DIR,
			includes: "../_includes",
			data: '../_data',
		}
	};
};