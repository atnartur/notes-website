FROM node:14-alpine

RUN apk add --no-cache nginx apache2-utils && mkdir -p /run/nginx

WORKDIR /app

COPY package.json . 
COPY package-lock.json .

RUN npm ci

COPY . .

RUN rm /etc/nginx/conf.d/default.conf && \
	ln -s /app/host.conf /etc/nginx/conf.d/default.conf && \
	sed -i -e 's/user nginx/user root/g' /etc/nginx/nginx.conf


CMD npm run build && \
	echo $AUTH_PASSWORD | htpasswd -i -c /etc/nginx/.htpasswd $AUTH_USER && \
	nginx -g "daemon off;"